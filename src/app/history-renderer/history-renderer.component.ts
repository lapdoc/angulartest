import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {HistoryItem} from '../models/historyItem';

@Component({
  selector: 'app-history-renderer',
  templateUrl: './history-renderer.component.html',
  styleUrls: ['./history-renderer.component.css']
})
export class HistoryRendererComponent implements OnInit {

  @Input() history: HistoryItem[] = [];
  @Output() searchEvent = new EventEmitter();

  constructor() { }

  ngOnInit() {}
  handleSearch(item) {
    this.searchEvent.emit(item);
  }

}
