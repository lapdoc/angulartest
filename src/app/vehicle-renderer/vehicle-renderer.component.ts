import {Component, OnInit, Input, SimpleChanges, Output, EventEmitter, OnChanges} from '@angular/core';
import { Vehicle } from '../models/vehicle';
import {StarwarsService} from '../starwars.service';
import {Film} from '../models/film';

@Component({
  selector: 'app-vehicle-renderer',
  templateUrl: './vehicle-renderer.component.html',
  styleUrls: ['./vehicle-renderer.component.css']
})
export class VehicleRendererComponent implements OnInit, OnChanges {

  @Input() model: Vehicle;
  @Output() filmLoadError = new EventEmitter();
  myFilm: Film;
  showFilm = false;

  constructor(private starwarsService: StarwarsService) { }

  ngOnInit() {
    window.scrollTo(0, document.body.scrollHeight);
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log(this.model);
    if (this.model.films[0]) {
      this.starwarsService.getUrl(this.model.films[0]).subscribe((data) => {
        this.myFilm = data;
        this.showFilm = true;
      }, error => {
        this.filmLoadError.emit('error');
      });
    } else {
      this.showFilm = false;
    }
  }

}
