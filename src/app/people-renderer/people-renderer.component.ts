import {Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter} from '@angular/core';
import { Person } from '../models/person';
import {Film} from '../models/film';
import {StarwarsService} from '../starwars.service';

@Component({
  selector: 'app-people-renderer',
  templateUrl: './people-renderer.component.html',
  styleUrls: ['./people-renderer.component.css']
})
export class PeopleRendererComponent implements OnInit, OnChanges {

  @Input() model: Person;
  @Output() filmLoadError = new EventEmitter();
  myFilm: Film;
  showFilm = false;

  constructor(private starwarsService: StarwarsService) { }

  ngOnInit() {
    window.scrollTo(0, document.body.scrollHeight);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.model.films[0]) {
      this.starwarsService.getUrl(this.model.films[0]).subscribe((data) => {
        console.log(data);
        this.myFilm = data;
        this.showFilm = true;
      }, error => {
        this.filmLoadError.emit('error');
      });
    } else {
      this.showFilm = false;
    }
  }

}
