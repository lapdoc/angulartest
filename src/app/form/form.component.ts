import {Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges} from '@angular/core';
import { StarwarsService } from '../starwars.service';
import {HistoryItem} from '../models/historyItem';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit, OnChanges {

  category = ['people', 'planets', 'vehicles', 'species', 'starships'];
  selNumber = 1;
  selCategory = 'people';

  @Input() search: HistoryItem;
  @Output() loadObject = new EventEmitter();

  constructor(private starwarsService: StarwarsService) { }

  ngOnInit() {}

  ngOnChanges(changes: SimpleChanges): void {
    if (this.search) {
      this.selCategory = this.search.category;
      this.selNumber = this.search.id;
      const links = document.getElementById('category-list').children;
      // @ts-ignore
      for (const link of links) {
        console.log(link.children[0].innerHTML);
        if (link.children[0].innerHTML !== this.selCategory) {
          link.children[0].classList.remove('button-pressed');
        } else {
          link.children[0].classList.add('button-pressed');
        }
      }
    }
  }

  getCategory(event, item) {
    this.selCategory = item;
    const links = event.target.parentNode.parentNode.children;
    for (const link of links) {
      link.children[0].classList.remove('button-pressed');
    }
    event.target.classList.add('button-pressed');
  }

  wrap(typeString: string, identifier: number, data: any) {
    return {
      type: typeString,
      id: identifier,
      obj: data
    };
  }

  confirmParam(category, id) {
    switch (category) {
      case 'people':
        this.starwarsService.getResult(category, id).subscribe((data) => {
          this.loadObject.emit(this.wrap('people', id, data));
        },
        error => {
          this.loadObject.emit('error');
        });
        break;
      case 'planets':
        this.starwarsService.getResult(category, id).subscribe((data) => {
          this.loadObject.emit(this.wrap('planets', id, data));
        },
        error => {
          this.loadObject.emit('error');
        });
        break;
      case 'vehicles':
        this.starwarsService.getResult(category, id).subscribe((data) => {
          this.loadObject.emit(this.wrap('vehicles', id, data));
        },
        error => {
          this.loadObject.emit('error');
        });
        break;
      case 'species':
        this.starwarsService.getResult(category, id).subscribe((data) => {
          this.loadObject.emit(this.wrap('species', id, data));
        },
        error => {
          this.loadObject.emit('error');
        });
        break;
      case 'starships':
        this.starwarsService.getResult(category, id).subscribe((data) => {
          this.loadObject.emit(this.wrap('starships', id, data));
        },
        error => {
          this.loadObject.emit('error');
        });
        break;
    }
  }

}
