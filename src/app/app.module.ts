import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { FormComponent } from './form/form.component';
import { PlanetRendererComponent } from './planet-renderer/planet-renderer.component';
import { SpaceshipRendererComponent } from './spaceship-renderer/spaceship-renderer.component';
import { VehicleRendererComponent } from './vehicle-renderer/vehicle-renderer.component';
import { PeopleRendererComponent } from './people-renderer/people-renderer.component';
import { SpeciesRendererComponent } from './species-renderer/species-renderer.component';
import { ErrorComponent } from './error/error.component';
import { HistoryRendererComponent } from './history-renderer/history-renderer.component';

@NgModule({
  declarations: [
    AppComponent,
    FormComponent,
    PlanetRendererComponent,
    SpaceshipRendererComponent,
    VehicleRendererComponent,
    PeopleRendererComponent,
    SpeciesRendererComponent,
    ErrorComponent,
    HistoryRendererComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
