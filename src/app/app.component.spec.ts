import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { FormComponent } from './form/form.component';
import { PlanetRendererComponent } from './planet-renderer/planet-renderer.component';
import { SpaceshipRendererComponent } from './spaceship-renderer/spaceship-renderer.component';
import { VehicleRendererComponent } from './vehicle-renderer/vehicle-renderer.component';
import { PeopleRendererComponent } from './people-renderer/people-renderer.component';
import { SpeciesRendererComponent } from './species-renderer/species-renderer.component';
import { HistoryRendererComponent } from './history-renderer/history-renderer.component';
import { ErrorComponent } from './error/error.component';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        FormComponent,
        PlanetRendererComponent,
        SpaceshipRendererComponent,
        VehicleRendererComponent,
        PeopleRendererComponent,
        SpeciesRendererComponent,
        ErrorComponent,
        HistoryRendererComponent
      ],
      imports: [
        BrowserModule,
        HttpClientModule,
        FormsModule
      ]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'AngularTest'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('AngularTest');
  });

});
