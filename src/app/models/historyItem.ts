export class HistoryItem {
  category: string;
  id: number;
  time: Date;
  constructor(category: string, id: number, date: Date ) {
    this.category = category;
    this.id = id;
    this.time = date;
  }
}
