import { Component, OnInit } from '@angular/core';
import {HistoryItem} from './models/historyItem';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'AngularTest';
  selectedObject: any;
  isError: boolean;
  history: HistoryItem[] = [];
  selectedHistoryItem: HistoryItem;

  toShow = {people: { show: false}, species: { show: false}, vehicles: { show: false}, planets: { show: false}, starships: { show: false}};


  constructor() {  }

  ngOnInit() {

  }

  setFalse() {
    this.toShow.people.show = false;
    this.toShow.species.show = false;
    this.toShow.vehicles.show = false;
    this.toShow.planets.show = false;
    this.toShow.starships.show = false;
  }

  searchEventHandler(item) {
    this.selectedHistoryItem = item;
  }

  loadObjectHandler(result: any) {
    if (result === 'error') {
      this.setFalse();
      this.isError = true;
      return;
    } else {
      this.isError = false;
    }

    this.selectedObject = result.obj;
    this.setFalse();
    const d = new Date();
    switch (result.type) {
      case 'people':
        this.history.push(new HistoryItem('people', result.id, d));
        this.toShow.people.show = true;
        break;
      case 'planets':
        this.history.push(new HistoryItem('planets', result.id, d));
        this.toShow.planets.show = true;
        break;
      case 'vehicles':
        this.history.push(new HistoryItem('vehicles', result.id, d));
        this.toShow.vehicles.show = true;
        break;
      case 'species':
        this.history.push(new HistoryItem('species', result.id, d));
        this.toShow.species.show = true;
        break;
      case 'starships':
        this.history.push(new HistoryItem('starships', result.id, d));
        this.toShow.starships.show = true;
        break;
    }
  }
}
