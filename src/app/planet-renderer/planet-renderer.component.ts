import {Component, OnInit, Input, Output, EventEmitter, SimpleChanges, OnChanges} from '@angular/core';
import { Planet } from '../models/planet';
import {Film} from '../models/film';
import {StarwarsService} from '../starwars.service';

@Component({
  selector: 'app-planet-renderer',
  templateUrl: './planet-renderer.component.html',
  styleUrls: ['./planet-renderer.component.css']
})
export class PlanetRendererComponent implements OnInit, OnChanges {

  @Input() model: Planet;
  @Output() filmLoadError = new EventEmitter();
  myFilm: Film;
  showFilm = false;

  constructor(private starwarsService: StarwarsService) { }

  ngOnInit() {
    window.scrollTo(0, document.body.scrollHeight);
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log(this.model);
    if (this.model.films[0]) {
      this.starwarsService.getUrl(this.model.films[0]).subscribe((data) => {
        this.myFilm = data;
        this.showFilm = true;
      }, error => {
        this.filmLoadError.emit('error');
      });
    } else {
      this.showFilm = false;
    }
  }

}
